require 'rdf'

# Common vocabulary for Gentoo Wiki properties
module GentooWiki
  NS_WIKI = 'http://wiki.gentoo.org/id/'
  NS_PROP = 'http://wiki.gentoo.org/id/Property-3A'
  NS_SWIVT = 'http://semantic-mediawiki.org/swivt/1.0#'

  SUBOBJECT = RDF::URI.new(NS_PROP + 'Has_subobject')

  PROJ = RDF::URI.new(NS_WIKI + 'Category-3AGentoo_Projects')

  PROP_NAME = RDF::URI.new(NS_PROP + 'Has_Name')
  PROP_CONTACT = RDF::URI.new(NS_PROP + 'Has_Contact')
  PROP_DESC = RDF::URI.new(NS_PROP + 'Has_Description')
  PROP_LEAD_ELECTION_DATE = RDF::URI.new(NS_PROP + 'Has_Lead_Election_Date')
  PROP_IRC = RDF::URI.new(NS_PROP + 'Has_IRC_channel')
  PROP_PARENT_PROJECT = RDF::URI.new(NS_PROP + 'Has_Parent_Project')
  PROP_PROPAGATES_MEMBERS = RDF::URI.new(NS_PROP + 'Propagates_Members')

  PROP_DEV = RDF::URI.new(NS_PROP + 'Has_Developer')
  PROP_ROLE = RDF::URI.new(NS_PROP + 'Has_Role')
  PROP_LEAD = RDF::URI.new(NS_PROP + 'Is_Project_Lead')

  PROP_NICK = RDF::URI.new(NS_PROP + 'Has_Nickname')

  # SWIVT properties
  PROP_PAGE = RDF::URI.new(NS_SWIVT + 'page')
end
