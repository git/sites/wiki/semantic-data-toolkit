#!/usr/bin/env ruby
# Gathers project information from the RDF graph into a JSON file.
# Alex Legler <a3li@gentoo.org>

require_relative 'lib/vocabulary'
require 'json'
require 'rdf/rdfxml'
require 'uri'
require 'cgi'
include RDF

abort "Usage: #{$PROGRAM_NAME} <RDF graph file or URL>" unless ARGV.first

graph = RDF::Graph.load(ARGV.first)

projects_q = Query.new do
  # Base information about the project
  pattern [:proj_uri, RDF.type, GentooWiki::PROJ]
  pattern [:proj_uri, GentooWiki::PROP_NAME, :proj_name]
  pattern [:proj_uri, GentooWiki::PROP_CONTACT, :proj_email]
  pattern [:proj_uri, GentooWiki::PROP_DESC, :proj_desc]
  pattern [:proj_uri, RDF::RDFS.label, :proj_title]
  pattern [:proj_uri, GentooWiki::PROP_PAGE, :proj_href]

  # Find parents
  pattern [:proj_uri, GentooWiki::PROP_PARENT_PROJECT, :parent_uri]
  pattern [:parent_uri, RDF::RDFS.label, :parent_title]

  pattern [:proj_uri, GentooWiki::PROP_PROPAGATES_MEMBERS, :propagates_members], optional: true
  pattern [:proj_uri, GentooWiki::PROP_IRC, :proj_irc], optional: true
  pattern [:proj_uri, GentooWiki::PROP_LEAD_ELECTION_DATE, :proj_lead_election_date], optional: true
end

def query_members(project_uri)
  Query.new do
    pattern [RDF::URI.new(project_uri), GentooWiki::SUBOBJECT, :member_subobj_uri]
    pattern [:member_subobj_uri, GentooWiki::PROP_DEV, :member_uri]
    pattern [:member_subobj_uri, GentooWiki::PROP_LEAD, :member_is_lead]
    pattern [:member_uri, GentooWiki::PROP_NICK, :member_nick]
    pattern [:member_uri, GentooWiki::PROP_NAME, :member_name]
    pattern [:member_uri, GentooWiki::PROP_CONTACT, :member_email]

    pattern [:member_subobj_uri, GentooWiki::PROP_ROLE, :member_roles], optional: true
  end
end

def label_to_id(label)
  label.to_s.gsub(/^Project:/, '')
end

def https(url)
  url.gsub(%r{^//}, 'https://')
end

def email(uri)
  uri.gsub(/^mailto:/, '')
end

projects = {}

projects_q.execute(graph) do |result|
  project_id = label_to_id result[:proj_title]

  project = {}
  project['name'] = result[:proj_name].to_s
  project['email'] = email result[:proj_email].to_s
  project['description'] = result[:proj_desc].to_s
  project['irc'] = result[:proj_irc].to_s
  project['href'] = CGI.unescape(https(result[:proj_href].to_s).gsub('-', '%'))
  project['propagates_members'] = result[:propagates_members].to_s == 'true'
  project['lead_election_date'] = result[:proj_lead_election_date].to_s

  parent_id = label_to_id result[:parent_title].to_s
  project['parent'] = parent_id unless parent_id == 'Gentoo'

  project['members'] = []
  query_members(result[:proj_uri]).execute(graph) do |member_result|
    member_hash = {
      'nickname' => member_result[:member_nick].to_s,
      'is_lead' => member_result[:member_is_lead].to_s == 'true',
      'email' => email(member_result[:member_email].to_s),
      'name' => member_result[:member_name].to_s
    }

    member_hash['role'] = member_result[:member_roles].to_s unless member_result[:member_roles].nil?
    project['members'] << member_hash
  end

  projects[project_id] = project
end

puts projects.sort.to_h.to_json
